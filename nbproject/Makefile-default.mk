#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/353813583/delay.o ${OBJECTDIR}/_ext/353813583/LCDbusy.o ${OBJECTDIR}/_ext/353813583/LCDcommand.o ${OBJECTDIR}/_ext/353813583/LCDcountedstring.o ${OBJECTDIR}/_ext/353813583/LCDinit.o ${OBJECTDIR}/_ext/353813583/LCDletter.o ${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o ${OBJECTDIR}/_ext/353813583/LCDputs.o ${OBJECTDIR}/_ext/353813583/LCDsend.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/353813583/delay.o.d ${OBJECTDIR}/_ext/353813583/LCDbusy.o.d ${OBJECTDIR}/_ext/353813583/LCDcommand.o.d ${OBJECTDIR}/_ext/353813583/LCDcountedstring.o.d ${OBJECTDIR}/_ext/353813583/LCDinit.o.d ${OBJECTDIR}/_ext/353813583/LCDletter.o.d ${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o.d ${OBJECTDIR}/_ext/353813583/LCDputs.o.d ${OBJECTDIR}/_ext/353813583/LCDsend.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/353813583/delay.o ${OBJECTDIR}/_ext/353813583/LCDbusy.o ${OBJECTDIR}/_ext/353813583/LCDcommand.o ${OBJECTDIR}/_ext/353813583/LCDcountedstring.o ${OBJECTDIR}/_ext/353813583/LCDinit.o ${OBJECTDIR}/_ext/353813583/LCDletter.o ${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o ${OBJECTDIR}/_ext/353813583/LCDputs.o ${OBJECTDIR}/_ext/353813583/LCDsend.o


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=30F4011
MP_LINKER_FILE_OPTION=,--script=p30F4011.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/353813583/delay.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/delay.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/delay.c  -o ${OBJECTDIR}/_ext/353813583/delay.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/delay.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/delay.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDbusy.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDbusy.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDbusy.c  -o ${OBJECTDIR}/_ext/353813583/LCDbusy.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDbusy.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDbusy.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDcommand.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDcommand.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcommand.c  -o ${OBJECTDIR}/_ext/353813583/LCDcommand.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDcommand.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDcommand.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDcountedstring.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcountedstring.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDcountedstring.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcountedstring.c  -o ${OBJECTDIR}/_ext/353813583/LCDcountedstring.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDcountedstring.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDcountedstring.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDinit.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDinit.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDinit.c  -o ${OBJECTDIR}/_ext/353813583/LCDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDinit.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDinit.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDletter.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDletter.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDletter.c  -o ${OBJECTDIR}/_ext/353813583/LCDletter.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDletter.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDletter.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDpulseEnableBit.c  -o ${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDputs.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDputs.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDputs.c  -o ${OBJECTDIR}/_ext/353813583/LCDputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDputs.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDputs.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDsend.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDsend.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDsend.c  -o ${OBJECTDIR}/_ext/353813583/LCDsend.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDsend.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDsend.o.d" $(SILENT) 
	
else
${OBJECTDIR}/_ext/353813583/delay.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/delay.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/delay.c  -o ${OBJECTDIR}/_ext/353813583/delay.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/delay.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/delay.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDbusy.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDbusy.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDbusy.c  -o ${OBJECTDIR}/_ext/353813583/LCDbusy.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDbusy.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDbusy.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDcommand.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDcommand.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcommand.c  -o ${OBJECTDIR}/_ext/353813583/LCDcommand.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDcommand.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDcommand.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDcountedstring.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcountedstring.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDcountedstring.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDcountedstring.c  -o ${OBJECTDIR}/_ext/353813583/LCDcountedstring.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDcountedstring.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDcountedstring.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDinit.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDinit.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDinit.c  -o ${OBJECTDIR}/_ext/353813583/LCDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDinit.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDinit.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDletter.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDletter.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDletter.c  -o ${OBJECTDIR}/_ext/353813583/LCDletter.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDletter.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDletter.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDpulseEnableBit.c  -o ${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDpulseEnableBit.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDputs.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDputs.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDputs.c  -o ${OBJECTDIR}/_ext/353813583/LCDputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDputs.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDputs.o.d" $(SILENT) 
	
${OBJECTDIR}/_ext/353813583/LCDsend.o: /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/353813583 
	@${RM} ${OBJECTDIR}/_ext/353813583/LCDsend.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  /home/jjmcd/Projects/PIC/dsPIC/LCDlib-4011.X/LCDsend.c  -o ${OBJECTDIR}/_ext/353813583/LCDsend.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/353813583/LCDsend.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/353813583/LCDsend.o.d" $(SILENT) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib-4011.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
