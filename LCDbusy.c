/*! \file LCDbusy.c
 *
 *  \brief Wait while LCD busy
 *
 *  Reads the LCD busy bit repeatedly until it becomes clear.  If
 *  it does not clear in a very long time (~600ms), leave anyway.
 *
 */

#include "lcd_intern.h"
#include "delay.h"

/*! Wait while LCD is busy                                              */
/*!  Monitor LCD busy flag until it becomes non-busy.  Note that
 *   we need to allow time for data to stabilize before testing.
 *
 * \param none
 * \returns none
 */
void LCDbusy( void )
{
  int nTimeSpent;
  int nBusy;

  /* Now, let's set the LCD to be read                                  */
  LCD_RS = 0;
  LCD_RW = 1;               /* Set r/w                                  */
  LCD_DATATRIS |= 0x00F0;   /* set data pins to read                    */

  /* Now read the LCD over and over and see what's going on.
   * We will wait 200 microseconds between each sample.  With 3000
   * samples that gives us 600ms.  Since the longest operation is
   * specced at 2ms, we should go from busy to not busy. If it takes
   * any longer, something is broken.                                   */
  nBusy=1;                          /* Assume busy                      */
  nTimeSpent = 0;                   /* Initialize counter               */
  while ( nBusy )
  {
      Delay (Delay200uS_count);     /* Do this at some reasonable rate  */
      LCD_ENABLE = 1;               /* Strobe out the high 4 bits       */
      Delay(12);                    /* 12 is the minimum delay          */
      if ( !(LCD_DATAPORT & 0x0080) )/* If the high bit is clear        */
          nBusy=0;                  /* we are no longer busy            */
      LCD_ENABLE = 0;               /* Drop the strobe                  */
      Delay(14);                    /* LCD needs time to see it         */

      /* Even though not used, we must strobe lower 4 bits out          */
      LCD_ENABLE = 1;               /* Strobe out the low 4 bits        */
      Delay(12);                    /* 12 is the minimum delay          */
      LCD_ENABLE = 0;               /* Drop the strobe                  */
      Delay(14);

      nTimeSpent++;                 /* Note how long we have been doing */
      if ( nTimeSpent>3000 )        /* this.  We don't want to wait     */
          nBusy=0;                  /* forever in case something broken */
  }

  /* Set the LCD back to normal writing */
  LCD_DATATRIS &= 0xFF0F;
  LCD_RW = 0;
  LCD_RS = 1;
}
