/*! \file LCDletter.c
 *
 *  \brief Send a character to the LCD
 *
 */

#include "lcd.h"
#include "lcd_intern.h"
#include "delay.h"


/*! Send a character to the LCD */
/*!  This routine simply sends a data byte to the LCD.  The
 *   register select pin is set to 1 notifying the LCD that the
 *   byte is to be used as a displayed character.
 * \param data char - Character to send to the LCD
 * \returns none
 *
 */
void LCDletter( char data )
{
    LCDbusy();
    LCD_RS = 1;         /* assert register select to 1 */
    LCDsend( data );
    LCD_RS = 1;         /* negate register select to 0 */
}
