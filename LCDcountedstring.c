/*! \file LCDcountedstring.c
 *
 *  \brief Send a specific number of characters to the LCD
 *
 */

#include "lcd.h"
#include "lcd_intern.h"
#include "delay.h"


/*! Send a counted string to the LCD */
/*! In C, strings are always terminated with a null character, so
 *  there is no need to count characters.  However, it is possible
 *  to send something else to the LCD, thinking it is a string when
 *  in fact, it is not terminated.  Therefore, in embedded
 *  applications, it is safer to count characters so the string
 *  display is guaranteed to complete no matter what the data.
 *
 * \param data unsigned char * - pointer to the string to be displayed
 * \param count int - count of number of characters to send to LCD
 * \returns none
*/
void LCDcountedstring( unsigned char *data, unsigned char count)
{
    while (count)
    {
        LCDletter(*data++);
        count--;
    }
}
