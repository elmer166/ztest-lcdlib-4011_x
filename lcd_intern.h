/*! \file lcd_intern.h
 *
 *  \brief Definitions used within LCD routines
 *
 * This file contains definitions of the various connections
 * to the LCD on the Explorer 16 board.  They are uninteresting
 * outside the LCD routines.
 */
/* 
 * File:   lcd_intern.h
 * Author: jjmcd
 *
 * Created on June 19, 2012, 12:57 PM
 */


#include <xc.h>

#ifndef LCD_INTERN_H
#define	LCD_INTERN_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
   For Explorer 16 board, here are the data and control signal definitions
   RS -> RB15
   E  -> RD4
   LCD_RW -> RD5
   DATA -> RE0 - RE7
*/

/* Control signal data pins */
/*! LCD Read/Write pin */
#define  LCD_RW         LATFbits.LATF1
/*! LCD Register select pin */
#define  LCD_RS         LATEbits.LATE8
/*! LCD Enable pin */
#define  LCD_ENABLE     LATFbits.LATF0

/* Control signal pin direction */
/*! LCD Read/Write direction register bit */
#define  LCD_RW_TRIS        TRISFbits.TRISF1
/*! LCD Register select direction register bit */
#define  LCD_RS_TRIS        TRISEbits.TRISE8
/*! LCD Enable direction register bit */
#define  LCD_ENABLE_TRIS    TRISFbits.TRISF0

/* Data signals and pin direction */
/*! LCD data port latch */
#define  LCD_DATA      LATB
/*! LCD data port */
#define  LCD_DATAPORT  PORTB
/*! LCD data port direction register */
#define  LCD_DATATRIS  TRISB

/* Definitions of LCD commands */
/*! LCD COMMAND - Clear LCD */
#define LCD_CLEAR       0x01
/*! LCD COMMAND - Move cursor home */
#define LCD_HOME        0x02

/*! LCD Entry Mode command */
#define LCD_ENTRY_MODE  0x04
/*! LCD Entry Mode command - increment address on data  */
#define LCD_ENT_INCR    0x02
/*! LCD Entry Mode command - decrement address on data */
#define LCD_ENT_DECR    0x00
/*! LCD Entry Mode command - shift display on data */
#define LCD_ENT_SHIFT   0x01

/*! LCD Display Command */
#define LCD_DISP        0x08
/*! LCD Display Command - turn display on */
#define LCD_DISP_ON     0x04
/*! LCD Display Command - turn display off */
#define LCD_DISP_OFF    0x00
/*! LCD Display Command - turn cursor on */
#define LCD_CURS_ON     0x02
/*! LCD Display Command - turn cursor off */
#define LCD_CURS_OFF    0x00
/*! LCD Display Command - turn blinking on */
#define LCD_BLINK_ON    0x01
/*! LCD Display Command - turn blinking off */
#define LCD_BLINK_OFF   0x00

/*! LCD shift command */
#define LCD_SHIFT       0x10
/*! LCD shift command - shift display */
#define LCD_SHIFT_DISP  0x08
/*! LCD shift command - shift cursor */
#define LCD_SHIFT_CURS  0x00
/*! LCD shift command - shift right */
#define LCD_SHIFT_RIGHT 0x04
/*! LCD shift command - shift left */
#define LCD_SHIFT_LEFT  0x00

/*! LCD Function Set Command */
#define LCD_FUN_SET     0x28
/*! LCD Function Set Command - 4 bit data length*/
#define LCD_4_BIT       0x00
/*! LCD Function Set Command - 8 bit data length */
#define LCD_8_BIT       0x10
/*! LCD Function Set Command - Select font set 0 Japanese (Newhaven OLED only) */
#define LCD_FONT0       0x00
/*! LCD Function Set Command - Select font set 1 European 1 (Newhaven OLED only) */
#define LCD_FONT1       0x01
/*! LCD Function Set Command - Select font set 2 Russian (Newhaven OLED only) */
#define LCD_FONT2       0x02
/*! LCD Function Set Command - Select font set 3 European 2 (Newhaven OLED only) */
#define LCD_FONT3       0x03
/*! LCD Function Set Command - Select two line display (non-OLED) */
#define LCD_2_LINE      0x08
/*! LCD Function Set Command - Select one line display (non-OLED) */
#define LCD_1_LINE      0x00
/*! LCD Function Set Command - Select 5x8 font (non-OLED) */
#define LCD_5X8_CHAR    0x04
/*! LCD Function Set Command - Select 5x11 font (non-OLED) */
#define LCD_5X11_CHAR   0x00


/*! Toggle the LCD enable bit */
void LCDpulseEnableBit( void );
/*! Wait for LCD not busy */
void LCDbusy( void );
/*! Send a byte to the LCD */
void LCDsend( char );

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_INTERN_H */

