/*! \file LCDpulseEnableBit.c
 *
 *  \brief Pulse the LCD enable bit for long enough
 *
 */

#include "delay.h"
#include "lcd_intern.h"

/*! Toggle the LCD enable bit */
/*! Each LCD command is strobed into the device by raising the
 * enable bit for at least 40 microseconds.  This routine
 * provides this function to the other functions in the
 * library.
 */
void LCDpulseEnableBit( void )
{
  /* Give data a chance to stabilize  */
  Delay(12); /* 12 is the minimum delay */

  LCD_ENABLE = 1;

  /* We need 450ns here. This gives us about 530ns @ 30MIPS, only a
   * little more than we need   */
  Delay(14);

  LCD_ENABLE = 0; /* toggle E signal */

}
